if (!RedactorPlugins) var RedactorPlugins = {};

RedactorPlugins.fontfamily = {
	init: function ()
	{
		var fonts = [   'Arial',
						'Helvetica',
						'Georgia',
						'Times New Roman',
						'Monospace',
						'HansHand',
						'Scriptina',
						'Cabrito',
						'Calluna',
						'Capita',
						'Flexo',
						'Foro',
						'Kuro',
						'Moderna',
						'Museo',
						'Thirsty'
		];

		var that = this;
		var dropdown = {};

		$.each(fonts, function(i, s)
		{
			dropdown['s' + i] = { title: "<font face='"+s+"'>"+s+"</font>", callback: function() { that.setFontfamily(s); }};
		});

		dropdown['remove'] = { title: 'Remove font', callback: function() { that.resetFontfamily(); }};

		this.buttonAdd('fontfamily', 'Change font family', false, dropdown);
	},
	setFontfamily: function (value)
	{
		this.inlineSetStyle('font-family', value);
	},
	resetFontfamily: function()
	{
		this.inlineRemoveStyle('font-family');
	}
};