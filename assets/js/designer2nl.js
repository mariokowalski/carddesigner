/**
 *
 * Falls mobile dann eine eigene draggable methode verwenden, die auf Touch ausgelegt ist. jQuery's draggable Methode ist dort leider etwas buggy.
 *
 */

var mobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? true : false;

if (mobile) {

    $.fn.mobileDraggable = function () {
        var offset = null;
        var start = function (e) {
            var orig = e.originalEvent;
            var pos = $(this).parent().position();
            offset = {
                x: orig.changedTouches[0].pageX - pos.left,
                y: orig.changedTouches[0].pageY - pos.top
            };
        };
        var moveMe = function (e) {
            e.preventDefault();
            var orig = e.originalEvent;

            newTop = (orig.changedTouches[0].pageY - offset.y) > 0 ? orig.changedTouches[0].pageY - offset.y : 0;
            newLeft = (orig.changedTouches[0].pageX - offset.x) > 0 ? orig.changedTouches[0].pageX - offset.x : 0;

            newLeft = newLeft < $("#postcard").width() - $(this).parent().outerWidth() ? newLeft : $("#postcard").width() - $(this).parent().outerWidth();
            newTop = newTop < $("#postcard").height() - $(this).parent().outerHeight() ? newTop : $("#postcard").height() - $(this).parent().outerHeight();


            $(this).parent().css({
                top: newTop,
                left: newLeft
            });
        };
        this.bind("touchstart", start);
        this.bind("touchmove", moveMe);
    };


}

$.fn.felixResizeable = function () {
    var offset = null;
    down = false;
    elem = $();
    var start = function (e) {
        var orig = e.originalEvent;
        var pos = $(this).parent().position();
        offset = {
            x: orig.clientX,
            y: orig.clientY,
            height: $(this).parent().height(),
            width: $(this).parent().width()
        };

        elem = $(this);

        // gesamt document binden, da sonst die handler nicht befuert werden wenn während des moves die maus aus der contentbox des .resizer elements heraustritt
        $(document).bind("mouseup", stop);
        $(document).bind("mousemove", moveMe);

        down = true;
    };
    var moveMe = function (e) {
        if(!down) return;
        e.preventDefault();
        var orig = e.originalEvent;

        newHeight = (orig.clientY - offset.y) + offset.height
        newWidth = (orig.clientX - offset.x) + offset.width

        if(($(elem).parent().position().left + newWidth + 22) > $("#postcard").width()) {
            newWidth = $("#postcard").width() - $(elem).parent().position().left - 22; // 22 = 2x .textdiv margin + 2x textdiv border
        }

        if(($(elem).parent().position().top + newHeight + 22) > $("#postcard").height()) {
            newHeight = $("#postcard").height() - $(elem).parent().position().top - 22; // 22 = 2x .textdiv margin + 2x textdiv border
        }

        $(elem).parent().css({
            height: newHeight,
            width: newWidth
        });
    };
    var stop = function(e) {
        e.preventDefault();
        down = false;
        $(document).unbind("mousmove");
        $(document).unbind("mouseup");

    };
    this.bind("mousedown", start);
};


$(document).ready(function () {
    // erstes Div auf die Karte werfen
    newDiv();

    $("#addText").click(function () {
        newDiv();
    });

    $("#imgfile").change(function () {
        addImg(this);
    });

    $("#imgWrapper").click(function () {
        $("#imgfile").click();
    })

    $(".option").click(function () {
        var img = new Image();
        img.onload = function () {
            $("#postcard").css({"background-image": "url(" + $(this).attr("src") + ")", "width": this.width, "height": this.height});
            $("#preview").css({"width": this.width, "height": this.height});
            $(".modal-dialog").css("width", this.width + 30 + "px"); // 30 = 2*15. padding von Elternelement = 15

        }
        img.src = $(this).attr("src");

    });
    $(".option").first().click();

    $(".clipart").click(function(){

        width = 250 * ($(this).get(0).clientWidth / $(this).get(0).clientHeight);


        $('#firstImgdiv').clone()                                                   // neues Image clonen
            .appendTo($("#postcard"))                                               // das jQ Objekt der Karte hinzufügen
            .removeAttr("id")                                                       // id leeren, da diese nicht doppelt sein darf
            .show()                                                                 // Bild auf gleiche position wie karte setzen
            .draggable({containment: "parent"})                                     // draggable
            .resizable({containment: "parent", aspectRatio: true, handles: "se"})   // resizable
            .css({"height": 250, width: width})
            .find("img")                                                            // und das Bild…
            .attr('src', $(this).attr("src"))                                       // …mit dem Motiv ausstatten
    })


    $("body").on("click", ".remover", function () {
        $(this).parent().remove();
    })


    $("#submit").click(function () { // Bild rendern wird nun angestoßen

        // Im Modal alles vorbereiten
        $("#spinner").show();
        $("#preview").hide();

        // Firefox macht Gänsefüsschen um die URL :@
        var url = $("#postcard").css("background-image").slice(4, -1); // "url(" und ")" rausschneiden
        if (url.charAt(0) == "\"") { // Falls der erste Char ein "
            url = url.slice(1, -1) // Die Gänsefüßchen rausschneiden
        }

        var obj = { // Das Objekt, welches an den Webserver übermittelt wird
            "background-image": url.split("/").slice(-1)[0], // nur letzen Teil des Pfads (den Dateinamen) nehmen
            dimensions: $("#postcard").dimensions(),
            elements: []

        }

        $("#postcard").children(":visible").each(function (index, item) { // Alle sichtbaren Objekte = Alle Objekte, die der Nutzer verändern/erstellen kann.

            if ($(item).hasClass("textdiv")) {
                textDiv = {
                    type: "text",
                    // RegEx ist vorhanden, damit die line-height auf dem Server korrekt geparst wird.
                    // Hier im HTML wird diese Eigenschaft vererbt, da Redactor nicht nativ (als style inline) damit umgehen kann.
                    // Daher hier die manuelle Einschreibung ins style-Tag

                    // letzteres Replace, damit keine non-Ascii-Zeichen auf dem Server falsch geparst werden
                    text: $("#redactor" + $(item).data("id")).redactor("get").replace(/style=\"font-size: (\d{2}px)/g, 'style=\"font-size: $1; line-height: $1').replace(/[^[:ascii:]]/g, ""),
                    dimensions: $(item).dimensions()
                }


                // Anpassungen, da HTML != PDF. Sorgt für korrekte Boxen
                textDiv.dimensions.width = $(item).find(".redactor_box").dimensions().width - 12;
                textDiv.dimensions.height = $(item).find(".redactor_box").dimensions().height - 12;
                textDiv.dimensions.top += 1;


                obj.elements.push(textDiv)

            } else if ($(item).hasClass("imgdiv")) {

                obj.elements.push({
                    type: "img",
                    data: $(item).children("img").attr("src"),
                    dimensions: $(item).dimensions()
                })

            }

        })


        $.ajax("phpimg/index.php", { // Weg damit
            type: "POST",
            data: {json: obj},
            // crossDomain: true, // Je nach Servereinstellung
            success: function (data) {
                $("#spinner").hide();
                var dataURL = "data:image/jpeg;base64," + data;

                // Bild wird nun angezeigt
                $("#preview").attr("src", dataURL);
                $("#preview").show();

            }



        })

        return true;

    });
    $("body").on("focus", ".redactor_textarea", function () {
        $(this).parent().parent().find(".toolbar").show(); // Toolbar anzeigen
    });


    $("body").on("focusout", ".redactor_textarea", function () {
        if (!$(this).parent().parent().find(".toolbar").is(":hover")) // Wenn der Mauszeiger über der Toolbar liegt,
            $(this).parent().parent().find(".toolbar").hide();       // soll dies natürlich nicht ausgeblendet werden
    });


});


jQuery.fn.addFeaturesToDiv = function () {
    div = $(this);
    id = Math.floor(Math.random() * 1000000 + 1); // id würfeln
    div.find(".toolbar").attr("id", "toolbar" + id) // Toolbar mit der id versehen
    if (!mobile) {
        div.draggable(
            {
                containment: "parent",
                handle: ".dragger",
                start: function (event, ui) {
                    console.log("start");
                },
                drag: function (event, ui) {
                    console.log("drag");
                },
                create: function (event, ui) {
                    console.log("create");
                },
                stop: function (event, ui) {
                    console.log("stop");
                }
            })
    } else {
        div.find(".dragger").mobileDraggable();
        $("#toolbar" + id).css({
            top: "16px",
            left: "-24px",
            width: "34px"
        })
    }/*
    div.resizable(
        {
            handles: "se",
            containment: "parent"

        })*/
    div.find(".resizer").felixResizeable()
    div.find("textarea").attr("id", "redactor" + id)
    div.data("id", id); // id speichern

    // textarea nun mit redactor versehen
    $("#redactor" + id).redactor({
        buttons: ['bold', 'aligncenter', 'alignleft'],
        toolbarExternal: "#toolbar" + id,
        plugins: ["fontfamily", "fontsize", "fontcolor"]
    });
    return div;
}

jQuery.fn.dimensions = function () {
    return {width: Math.round($(this).width()), height: Math.round($(this).height()), left: Math.round($(this).position().left), top: Math.round($(this).position().top)}
}

function newDiv() {
    $("#firstTextdiv").clone().appendTo($("#postcard")).removeAttr("id").show().css({top: "20px", left: "20px"}).addFeaturesToDiv();
}

function addImg(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#firstImgdiv').clone()                                                   // neues Image clonen
                .appendTo($("#postcard"))                                               // das jQ Objekt der Karte hinzufügen
                .removeAttr("id")                                                       // id leeren, da diese nicht doppelt sein darf
                .show()                                                                 // Bild auf gleiche position wie karte setzen
                .draggable({containment: "parent"})                                     // draggable
                .resizable({containment: "parent", aspectRatio: true, handles: "se"})   // resizable
                .find("img")                                                            // und das Bild…
                .attr('src', e.target.result)                                           // …mit dem Userbild ausstatten
                .css({"max-width": $("#postcard").width(),
                    "max-height": $("#postcard").height()});                          // Höchstens so hoch wie die Postkarte

        }

        reader.readAsDataURL(input.files[0]);
    }
}