//noinspection JSUnusedGlobalSymbols
var Labelizr = {

    mobile: !!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),

    postcard: $("#postcard"),

    init: function (){
        Labelizr.initjQExtensions();
        Labelizr.bindEvents();

    },

    bindEvents: function (){

        $("#addText").click(function ( e ){
            Labelizr.addTextDiv();
            e.preventDefault();
            return false;
        });

        if(Labelizr.mobile) {
            function doOnOrientationChange()
            {
                switch(window.orientation)
                {
                    case -90:
                    case 90:
                        $("#viewport").attr("content", "width="+window.screen.height+", initial-scale=1, maximum-scale=1")
                        break;
                    default:
                        $("#viewport").attr("content", "width="+window.screen.width+", initial-scale=0.75, maximum-scale=0.75")
                        break;
                }
            }
            window.addEventListener('orientationchange', doOnOrientationChange);
            doOnOrientationChange();

            $(".dragger, .resizer").hide();

            $(".remover").attr("style", "visibility: visible !important");

            var oldHeight, oldWidth, oldLeft, oldTop, img;
            $(document).hammer().on("transformstart", function ( e ){
                if(!$(e.target).parent().hasClass("imgDiv"))
                    return;
                img = $(e.target).parent();
                oldHeight = img.height();
                oldWidth = img.width();
                oldLeft = parseInt(img.css("left"), 10);
                oldTop = parseInt(img.css("top"), 10);
                // Disable scrolling. fixes bugs.

                $(document).on("touchmove.labelizr", function(e){e.preventDefault()});

            }).on("pinch", function(e){
                if(typeof img == "undefined")
                    return;
                var newHeight = oldHeight * e.gesture.scale;
                var newWidth = oldWidth * e.gesture.scale;
                var newTop = oldTop-(newHeight-oldHeight)/2;
                var newLeft = oldLeft-(newWidth-oldWidth)/2;
/*
                newLeft = e.gesture.center.clientX - oldLeft;
                newTop = e.gesture.center.clientY - oldTop;
                console.log(e);
*/
                img.css({height: newHeight, width: newWidth, left: newLeft, top: newTop});

            }).on("transformend", function(e){
                //re-enable scrolling
                $('html').css('overflow', "visible");
                img = undefined;
                $(document).off("touchmove.labelizr");
            });
        }

        $("#imgfile").change(function (){
            if ( this.files && this.files[0] ) {
                var reader = new FileReader();

                reader.onload = function ( e ){
                    var image = new Image();
                    image.src = e.target.result;

                    image.onload = function (){
                        // access image size here
                        Labelizr.addImageDiv(this.src, this.width, this.height);
                    };
                };

                reader.readAsDataURL(this.files[0]);
            }

            // reset of input[type=file] field
            $(this).wrap('<form>').closest('form').get(0).reset();
        });

        $("#imgWrapper").click(function (){
            $("#imgfile").click();
        });

        $(".clipart").click(function (){
            if ( $(this).hasClass("ui-draggable-dragging") && !$(this).hasClass("dropped") )
                return;

            Labelizr.addImageDiv($(this).attr("src"), $(this).get(0).clientWidth, $(this).get(0).clientHeight);

        }).draggable({revert: true, scroll: false});

        Labelizr.postcard.droppable({

            accept: ".clipart,.option",
            drop: function ( event, ui ){
                $(ui.draggable).addClass("dropped");
                if ( $(ui.draggable).hasClass("option") )
                    $(ui.draggable).click();
                else if ( $(ui.draggable).hasClass("clipart") )
                    $(ui.draggable).click();
                $(ui.draggable).removeClass("dropped");
            }

        });


        $(".option").click(function (){
            if ( $(this).hasClass("ui-draggable-dragging") && !$(this).hasClass("dropped") )
                return;

            var img = new Image();
            img.onload = function (){
                Labelizr.postcard.css({"background-image": "url(" + $(this).attr("src") + ")", "width": this.width, "height": this.height});
                $("#preview").css({"width": this.width, "height": this.height});
                $(".modal-dialog").css("width", this.width + 30 + "px"); // 30 = 2*15. padding from parent element = 15

            };
            img.src = $(this).attr("src");

        }).draggable({revert: true, scroll:false}).first().click();


        $("#submit").click(function (){
            Labelizr.preview();
        });

        $("body").on("click", ".remover", function (){
            $(this).parent().remove();
        })
            .on("focus", ".redactor_textarea", function (){
                $(this).parent().parent().find(".toolbar").show(); // show toolbar
            })
            .on("focusout", ".redactor_textarea", function (){
                if ( !$(this).parent().parent().find(".toolbar").is(":hover") ) // If cursor is over .toolbar,
                    $(this).parent().parent().find(".toolbar").hide();          // .toolbar should not be hidden
            });

    },

    preview: function (){
        // prepare everything inside the modal
        $("#spinner").show();
        $("#preview").hide();

        $.ajax("phpimg/index.php", { // aaand its gone
            type: "POST",
            data: {json: Labelizr.getLabelizrObject()},
            // crossDomain: true, // Je nach Servereinstellung
            success: function ( data ){
                $("#spinner").hide();
                var dataURL = "data:image/jpeg;base64," + data;

                // Bild wird nun angezeigt
                $("#preview").attr("src", dataURL).show();

            }



        });

        return true;

    },

    getLabelizrObject: function (){
        var url = Labelizr.postcard.css("background-image").slice(4, -1); // "url(" and ")" cutting
        // Firefox warps the url in -> " <- :@
        if ( url.charAt(0) == "\"" ) { // In case the first char is "
            url = url.slice(1, -1); // cut the "
        }

        var obj = { // The object what will be transmitted to the server
            "background-image": url.split("/").slice(-1)[0], // only choose the last part of url
            dimensions: Labelizr.postcard.dimensions(),
            elements: []

        };

        Labelizr.postcard.children(":visible").each(function ( index, item ){ // all visible objects = all user editable objects

            if ( $(item).hasClass("textDiv") ) {
                var textDiv = {
                    type: "text",
                    // RegEx ist vorhanden, damit die line-height auf dem Server korrekt geparst wird.
                    // Hier im HTML wird diese Eigenschaft vererbt, da Redactor nicht nativ (als style inline) damit umgehen kann.
                    // Daher hier die manuelle Einschreibung ins style-Tag

                    // letzteres Replace, damit keine non-Ascii-Zeichen auf dem Server falsch geparst werden
                    text: $("#redactor" + $(item).data("id")).redactor("get").replace(/style=\"font-size: (\d{2}px)/g, 'style=\"font-size: $1; line-height: $1').replace(/[^[:ascii:]]/g, ""),
                    dimensions: $(item).dimensions()
                };


                // Adjustments. HTML != PDF…
                textDiv.dimensions.width = $(item).find(".redactor_box").dimensions().width - 12;
                textDiv.dimensions.height = $(item).find(".redactor_box").dimensions().height - 12;
                textDiv.dimensions.top += 1;

                obj.elements.push(textDiv)

            } else if ( $(item).hasClass("imgDiv") ) {

                obj.elements.push({
                    type: "img",
                    data: $(item).children("img").attr("src"),
                    dimensions: $(item).dimensions()
                })

            }

        });

        return obj;
    },

    initjQExtensions: function (){
        $.fn.mobileDraggable = function (){
            var offset = null;
            var start = function ( e ){
                var orig = e.originalEvent;
                var pos = $(this).parent().position();
                offset = {
                    x: orig.changedTouches[0].pageX - pos.left,
                    y: orig.changedTouches[0].pageY - pos.top
                };
            };
            var moveMe = function ( e ){
                e.preventDefault();
                var orig = e.originalEvent;

                var newTop = (orig.changedTouches[0].pageY - offset.y) > 0 ? orig.changedTouches[0].pageY - offset.y : 0;
                var newLeft = (orig.changedTouches[0].pageX - offset.x) > 0 ? orig.changedTouches[0].pageX - offset.x : 0;

                newLeft = newLeft < Labelizr.postcard.width() - $(this).parent().outerWidth() ? newLeft : Labelizr.postcard.width() - $(this).parent().outerWidth();
                newTop = newTop < Labelizr.postcard.height() - $(this).parent().outerHeight() ? newTop : Labelizr.postcard.height() - $(this).parent().outerHeight();


                $(this).parent().css({
                    top: newTop,
                    left: newLeft
                });
            };
            this.bind("touchstart", start);
            this.bind("touchmove", moveMe);
        };


        $.fn.felixResizeable = function (){
            var offset = null;
            var down = false;
            var elem = $();
            var start = function ( e ){
                var orig = e.originalEvent;

                if ( event.type == "touchstart" ) {
                    offset = {
                        x: orig.touches[0].pageX,
                        y: orig.touches[0].pageY,
                        height: $(this).parent().height(),
                        width: $(this).parent().width()
                    };
                } else {
                    offset = {
                        x: orig.clientX,
                        y: orig.clientY,
                        height: $(this).parent().height(),
                        width: $(this).parent().width()
                    };
                }
                elem = $(this);

                // Bind the whole docuement prevent usability bugs
                $(document).bind("mouseup touchend", stop);
                $(document).bind("mousemove touchmove", moveMe);

                down = true;
            };
            var moveMe = function ( e ){
                if ( !down ) return;
                e.preventDefault();
                var orig = e.originalEvent;
                var newHeight, newWidth;
                if ( event.type == "touchmove" ) {

                    newHeight = (orig.touches[0].pageY - offset.y) + offset.height;
                    newWidth = (orig.touches[0].pageX - offset.x) + offset.width;

                } else {

                    newHeight = (orig.clientY - offset.y) + offset.height;
                    newWidth = (orig.clientX - offset.x) + offset.width;


                }
                if ( ($(elem).parent().position().left + newWidth + 22) > Labelizr.postcard.width() ) {
                    newWidth = Labelizr.postcard.width() - $(elem).parent().position().left - 22; // 22 = 2x .textDiv margin + 2x textDiv border
                }

                if ( ($(elem).parent().position().top + newHeight + 22) > Labelizr.postcard.height() ) {
                    newHeight = Labelizr.postcard.height() - $(elem).parent().position().top - 22; // 22 = 2x .textDiv margin + 2x textDiv border
                }

                $(elem).parent().css({
                    height: newHeight,
                    width: newWidth
                });
            };
            var stop = function ( e ){
                e.preventDefault();
                down = false;
                $(document).unbind("mousmove touchmove");
                $(document).unbind("mouseup touchend");

            };
            this.bind("mousedown touchstart", start);
        };


        jQuery.fn.dimensions = function (){
            return {width: Math.round($(this).width()), height: Math.round($(this).height()), left: Math.round($(this).position().left), top: Math.round($(this).position().top)}
        }


    },

    /**
     *
     * @param text optional Default Text
     * @param w optional Width of textfield
     * @param h optional height of textfield
     * @param x optional vertical pixel from upper left corner
     * @param y optional horizontal pixel from upper left corner
     */
    addTextDiv: function ( text, w, h, x, y ){
        x = x || 20;
        y = y || 20;
        h = h || 160;
        w = w || 160;
        text = text || "Your text";

        var newTextDiv = $("#firstTextdiv").clone()
            .appendTo(Labelizr.postcard)
            .removeAttr("id")
            .show()
            .css({top: y, left: x, width: w, height: h});

        // prepare toolbar
        var id = Math.floor(Math.random() * 1000000 + 1); // get random id
        newTextDiv.find(".toolbar").attr("id", "toolbar" + id); // put id in toolbar

        // Dragging fix für Mobile
        if ( !Labelizr.mobile ) {
            newTextDiv.draggable(
                {
                    containment: "parent",
                    handle: ".dragger"
                })
        } else {
            newTextDiv.find(".dragger").mobileDraggable();
            $("#toolbar" + id).css({
                top: "16px",
                left: "-24px",
                width: "34px"
            })
        }

        // add functions to resizer
        newTextDiv.find(".resizer").felixResizeable();

        // add id to textare for refinding
        newTextDiv.find("textarea").attr("id", "redactor" + id);
        newTextDiv.data("id", id); // id save

        // put redactor on textarea
        $("#redactor" + id).redactor({
            buttons: ['bold', 'aligncenter', 'alignleft'],
            toolbarExternal: "#toolbar" + id,
            plugins: ["fontfamily", "fontsize", "fontcolor"]
        });
        $("#redactor" + id).redactor('insertHtml', '<span style="font-size: 18px;">'+text+'</span>')

    },

    /**
     *
     * @param src required DataUrl like here: http://en.wikipedia.org/wiki/Data_URI_scheme
     * @param w required Width of image
     * @param h required height of image
     * @param x optional vertical pixel from upper left corner
     * @param y optional horizontal pixel from upper left corner
     */

    addImageDiv: function ( src, w, h, x, y ){

        x = x || 0;
        x = x || 0;

        var base = 250;
        base = Labelizr.postcard.width() < base ? Labelizr.postcard.width() : base;
        base = Labelizr.postcard.height() < base ? Labelizr.postcard.height() : base;


        if ( h > w ) {
            var width = base * (w / h);
            var height = base;
        } else {
            var height = base * (h / w);
            var width = base;
        }

        $('#firstImgdiv').clone()                                                   // clone new Image
            .appendTo(Labelizr.postcard)                                            // add jQ object to card
            .removeAttr("id")                                                       // clear id cause there must not be two same ids
            .show()                                                                 // show picture
            .draggable({containment: "parent"})                                     // draggable
            .resizable({containment: "parent", aspectRatio: true, handles: "se", "minHeight": 25, "minWidth": 25})   // resizable
            .css({"height": height, width: width, top:y, left:x})
            .find("img")                                                            // find img…
            .attr('src', src);                                                      // …and put userimg in it


    }


};

