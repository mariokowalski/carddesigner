<?php
date_default_timezone_set("Europe/Berlin");

require_once("tcpdf/tcpdf.php");


function get_string_between($string, $start, $end)
{
    $string = " " . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}


/* PHP START */

session_start();

if (!isset($_SESSION['id']) && empty($_SESSION["id"])) {
    $_SESSION["id"] = mt_rand() . mt_rand();
} else {

    //clear all files

    $files = glob("tmp/" . $_SESSION["id"] . "/*"); // get all file names
    foreach ($files as $file) { // iterate files
        if (is_file($file))
            unlink($file); // delete file
    }
}

$sid = $_SESSION["id"];
$tmpFolder = "tmp/" . $sid . "/";

if (!(file_exists($tmpFolder) || is_dir($tmpFolder))) {
    mkdir($tmpFolder);
}

$request = $_REQUEST["json"];

if ($request["dimensions"]["height"] > $request["dimensions"]["width"]) {
    $mode = "P";
} else {
    $mode = "L";
}


// Zwei Durchläufe. Das erste ohne Hintergrundbild, letzeres mit.

for ($i = 0; $i < 2; $i++) {

    $pdf = new TCPDF($mode, "pt", array($request["dimensions"]["width"], $request["dimensions"]["height"]));
    $pdf->SetAutoPageBreak(false);

    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    $pdf->AddPage();

    if ($i == 1) {
        $backgroundImagePath = "backgroundpictures/300dpi/" . $request["background-image"];
        if (file_exists($backgroundImagePath)) {
            $pdf->Image($backgroundImagePath, 0, 0, $request["dimensions"]["width"], $request["dimensions"]["height"]);
        }
        $pdf->setPageMark();
    }

    foreach ($request["elements"] as $key => $element) {
        if ($element["type"] == "img") {
            if (strpos($element["data"], "cliparts") !== false) {
                $pdf->Image("cliparts/".array_pop(explode('/', $element["data"])), $element["dimensions"]["left"], $element["dimensions"]["top"], $element["dimensions"]["width"], $element["dimensions"]["height"]);
            } else {
                $tmpImg = mt_rand() . mt_rand();
                $tmpImg .= "." . get_string_between($element["data"], "/", ";"); // get file extension (png or jpg)
                $tmpImg = $tmpFolder . $tmpImg;
                $array = explode(",", $element["data"]);
                file_put_contents($tmpImg, base64_decode(array_pop($array)));

                $pdf->Image($tmpImg, $element["dimensions"]["left"], $element["dimensions"]["top"], $element["dimensions"]["width"], $element["dimensions"]["height"]);
                unlink($tmpImg);
            }
        } elseif
        ($element["type"] == "text"
        ) {
            $pdf->WriteHTMLCell($element["dimensions"]["width"], $element["dimensions"]["height"], $element["dimensions"]["left"] + 15, $element["dimensions"]["top"] + 15, $element["text"]);
        }
    }

    $tmpPDF = $tmpFolder . $sid . $i . ".pdf";
    $pdf->Output($tmpPDF, "F");
}

// $tmpPDF ist immer noch der Path mit der PDF mit Hintergrundbild

exec("'/usr/local/bin/gm' convert +profile '*' -colorspace RGB -quality 100 '" . $tmpPDF . "[0]' '" . $tmpPDF . ".jpeg'");

echo base64_encode(file_get_contents($tmpPDF . ".jpeg"));


?>