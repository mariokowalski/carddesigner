<?php
date_default_timezone_set("Europe/Berlin");

/* PHP START */

session_start();

if(!isset($_SESSION["id"]) && empty($_SESSION["id"])) {
	$_SESSION["id"] = mt_rand().mt_rand();
	exit();
}

$sid = $_SESSION["id"];
$pdf = "tmp/".$sid."/".$sid."1.pdf";

if(file_exists($pdf)) {
	// Wir werden eine PDF Datei ausgeben
	header('Content-type: application/pdf');

	// Es wird downloaded.pdf benannt
	header('Content-Disposition: attachment; filename="downloaded.pdf"');

	readfile($pdf);
} else {
    echo "Datei nicht gefunden";
}

?>